import os
import sys
import datetime

import requests
import numpy as np

import tarfile
import pydicom
import pandas as pd


# download file
def download_file(url):
    try:
        local_filename = url.split('/')[-1]
        # NOTE the stream=True parameter below
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(local_filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
                        # f.flush()
        return local_filename
    except ValueError:
        print("error download the file")
        sys.exit()


# extract file
def untar(fname, dir):
    if (fname.endswith("tgz")):
        tar = tarfile.open(fname)
        tar.extractall(path=dir)
        tar.close()
        print("Extracted tar file in  Directory")
    else:
        print("Not a tar.gz file")


# create dir on the OS if no exsits
def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


# create the new path for each file accoording to patient/study/series
def get_folder_path(full_path, dcm_dir):
    os.chdir(dcm_dir)
    # for each file path create dir if not exsists
    for dir_p in full_path:
        create_dir(dir_p)
        os.chdir(os.path.join(os.getcwd(), dir_p))
    # get path & swith to base folder
    currenet_path = os.getcwd()
    os.chdir(dcm_dir)
    return currenet_path


# get patient age & normalize to years (int)
def normalize_age(full_age):
    type = full_age[3:]
    if type == 'Y':
        return int(full_age[0:3])
    elif type == 'M':
        return int(full_age[0:3])/12
    else:
        return 0


# create date time of dcm file based based on horos viewer time&date model
def create_creation_date(date, time):
    try:
        return datetime.datetime.strptime(date+' '+time, '%Y%m%d %H%M%S')
    except ValueError:
        return None

# update df with new row


def insert_row_to_df_dcm(PatientName, StudyInstanceUID, SeriesInstanceUID, PatientAge, InstitutionName, CreationDate, PatientSex, Path):
    row = len(df_dcm.index)
    df_dcm.loc[row] = ([PatientName, StudyInstanceUID, SeriesInstanceUID,
                        PatientAge, InstitutionName, CreationDate, PatientSex, Path])


# extract dcm file info plus move file to folder
def extract_dcm_file_data_plus_arrange_path(file_path, file_name):
    ds = pydicom.dcmread(file_path)
    new_dir_path = get_folder_path([str(ds.PatientName), str(
        ds.StudyInstanceUID), str(ds.SeriesInstanceUID)], dcm_dir)
    new_file_path = os.path.join(new_dir_path, file_name)
    os.rename(file_path, new_file_path)
    # insert all dcm info into the df
    insert_row_to_df_dcm(ds.PatientName, ds.StudyInstanceUID, ds.SeriesInstanceUID, normalize_age(str(ds.PatientAge)),
                         ds.InstitutionName, create_creation_date(str(ds.StudyDate), str(ds.AcquisitionTime)[:6]), ds.PatientSex, new_file_path)


# go all over dcm files  - extract info plus rearrrange files
def rearrange_files_and_extract_data():
    for filename in os.listdir(dcm_dir):
        if ".dcm" in filename.lower():  # check whether the file's DICOM
            extract_dcm_file_data_plus_arrange_path(
                os.path.join(dcm_dir, filename), filename)
    # return to main dir
    os.chdir(dcm_dir)


# adjust df columns to proper types plus extract log fiel
def adjust_df_columns_plus_extrect_logs(df_dcm, logs_file_name):
    # df_dcm.head()
    df_dcm.PatientName = df_dcm.PatientName.astype(np.str)
    df_dcm.PatientAge = df_dcm.PatientAge.astype(np.int64)
    df_dcm.PatientSex = df_dcm.PatientSex.astype(np.str)
    df_dcm.to_csv(logs_file_name+".csv", sep=',',
                  encoding='utf-8', index=False)


# Q 1
def first_question():
    df_Q1_list = pd.DataFrame(df_dcm.groupby(
        ['PatientName', 'PatientAge', 'PatientSex']).size()).reset_index()
    print("Q1 answer:")
    print(df_Q1_list[['PatientName', 'PatientAge', 'PatientSex']])


# Q 2
def second_question():
    print("answer for Q2")
    # group by the data in order to find scan time
    df_agg_scan_time = df_dcm.groupby(['PatientName', 'StudyInstanceUID', 'SeriesInstanceUID']).agg({
        'CreationDate': ['min', 'max']}).reset_index()
    df_agg_scan_time.columns = df_agg_scan_time.columns.droplevel()
    df_agg_scan_time['scan_time'] = df_agg_scan_time.apply(
        lambda row: row['max']-row['min'], axis=1)
    print("the average time per scan: ", df_agg_scan_time['scan_time'].mean())


# Q 3
def third_question():
    print("answer for Q3")
    unique_Institution_list = df_dcm.InstitutionName.unique()
    print("the number of unique Institution: ", len(unique_Institution_list))
    print(unique_Institution_list)


'''*********** Main ***********'''
if __name__ == "__main__":
    print("start script")
    # extract url arg
    print("extract arg")
    url = sys.argv[1]
    print(url)
    # download file & get the download file name
    print("download file")
    file_name = download_file(url)

    # remove the suffix of the file name in order to create dir with taht name
    print("create dir to extract files")
    directory = file_name[:file_name.find('.tgz')]
    # create_dir
    create_dir(directory)
    # extract files
    print("start extract files")
    untar(file_name, directory)

    # get the script file path
    base_dir = (os.path.abspath(''))
    # full path for the created file
    dcm_dir = os.path.join(base_dir, directory)

    # create df for dcm
    print("create df to hold files info")
    df_dcm = pd.DataFrame(columns=['PatientName', 'StudyInstanceUID', 'SeriesInstanceUID',
                                   'PatientAge', 'InstitutionName', 'CreationDate', 'PatientSex', 'Path'])

    print("rearrange files in the correct path plus extract files data to df")
    # go all over the files rearrange in the correct new path and extract file info
    rearrange_files_and_extract_data()

    # adjust df columns to proper types plus extract log fiel
    print("extract log file")
    adjust_df_columns_plus_extrect_logs(df_dcm, directory+"_logs")

    # Q 1
    first_question()

    # Q 2
    second_question()

    # Q 3
    third_question()
    # script end
    print("script end")
